package com.digitalroute.exercise1

import scala.io.StdIn.readLine
/**
  * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
object Application extends App {
  println(s"Running Application with $args")

  val accumulated = readLine("Accumulated purchase value: ").toDouble //.asInstanceOf[Double]
  val current = (readLine("Purchase amount: ")).toDouble //.asInstanceOf[Double]
  println(s"You wrote: $accumulated, $current") //To remove
  val calculator: Calculator = new Calculator(accumulated)

  println(s"Amount to pay: ${calculator.amountToPay(current)}")
  println(s"New accumulated purchase value:${accumulated+current}")

}
