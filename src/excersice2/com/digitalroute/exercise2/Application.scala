package com.digitalroute.exercise2

import scala.io.StdIn.readLine
import scala.util.{Failure, Success, Try}

/**
 * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
object Application extends App {
  println(s"Running Application with ${args.length}")

  val accumulated: Double = getPrintedValue("Accumulated purchase value: ", doubleValidator)
  val current = getPrintedValue("Purchase amount: ", doubleValidator)
  println(s"You wrote: $accumulated, $current")
  //To remove
  val calculator: Calculator = new Calculator(accumulated)

  println(s"Amount to pay: ${calculator.amountToPay(current)}")
  println(s"New accumulated purchase value:${accumulated + current}")

  def doubleValidator(input: String): Boolean = {
    Try(input.toDouble) match {
      case Success(v) => {
        if (v >= 0) true
        else {
          println(s"Amount must be 0 or greater. $v")
          false
        }
      }
      case Failure(t) => {
        println(s"Wrong value type. Need to be a number, $input")
        false
      }
    }
  }

  def getPrintedValue(text: String, validator: (String) => Boolean): Double = {
    val line: String = readLine(text)
    if (validator(line)) {
      line.toDouble
    } else {
      getPrintedValue(text, validator)
    }
  }

}
