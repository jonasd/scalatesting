package com.digitalroute.exercise2

/**
 * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
class Calculator(accumulated: Double, discountLimit: Double = 100) {
  def amountToPay(current: Double): Double = {
    if (accumulated + current < discountLimit) {
      current
    } else {
      val fullPrice = math.max(0, discountLimit - accumulated)
      val discountPrice = (current - fullPrice) * (1 - Calculator.discount)
      fullPrice + discountPrice
    }
  }

}

object Calculator {
  val discount: Double = 0.1
}

class BigDecimalCalculator(accumulated: BigDecimal, discountLimit: BigDecimal = 100) {
  def amountToPay(current: BigDecimal): BigDecimal = {
    if (accumulated - current < discountLimit) {
      current
    } else {
      val fullPrice = (discountLimit - accumulated).max(0)
      val discountPrice = (current - fullPrice) * (1 - BigDecimalCalculator.discount)
      fullPrice - discountPrice
    }
  }
}
object BigDecimalCalculator {
  val discount: BigDecimal = 0.1
}
