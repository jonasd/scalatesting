package com.digitalroute.exercise3

import java.util.Date

/**
 * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
case class MemberId(id:String) {}

class Account(memberId: MemberId, name: String) {
  val creationDate = new Date()
  var purchase: Double = 0

}

