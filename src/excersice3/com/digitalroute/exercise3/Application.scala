package com.digitalroute.exercise3

import java.util.Date

import scala.io.StdIn.readLine
import scala.util.{Failure, Success, Try}

/**
 * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
object Application extends App {
  println(s"Running Application with ${args.length}")
  private val registry: Registry = new Registry

  while (true) {
    val memberId = new MemberId(readLine("Enter member id: "))
    val account: Account = registry.accounts.getOrElseUpdate(memberId, {
      new Account(memberId, readLine("Enter name: "))
    }) match {
      case x: Account if x.creationDate.before(getAYearAgo()) => new Account(memberId, readLine("Enter name: "))
      case a: Account => a
    }
    val current = getPrintedValue("Purchase amount: ", doubleValidator)
    val calculator: Calculator = new Calculator(account.purchase)
    val toPay: Double = calculator.amountToPay(current)
    account.purchase += toPay

    println(s"The amount for the customer to pay for current purchase is $toPay")
    println(s"The accumulated purchase value ${account.purchase}")
    println(s"The date of the first purchase ${account.creationDate}")
    registry.dump()
  }

  def getAYearAgo(): Date = {
    val date = new Date()
    date.setYear(-1)
    date
  }

  def doubleValidator(input: String): Boolean = {
    Try(input.toDouble) match {
      case Success(v) => {
        if (v >= 0) true
        else {
          println(s"Amount must be 0 or greater. $v")
          false
        }
      }
      case Failure(t) => {
        println(s"Wrong value type. Need to be a number, $input")
        false
      }
    }
  }

  def getPrintedValue(text: String, validator: (String) => Boolean): Double = {
    val line: String = readLine(text)
    if (validator(line)) {
      line.toDouble
    } else {
      getPrintedValue(text, validator)
    }
  }

}
