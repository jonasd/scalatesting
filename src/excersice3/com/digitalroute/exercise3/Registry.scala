package com.digitalroute.exercise3

import scala._
import scala.collection.mutable._

/**
 * Copyright (c) 01/02/16 DigitalRoute
 * All rights reserved.
 * Usage of this program and the accompanying materials is subject to license terms
 *
 * @author jonas.damfors
 */
class Registry {

  val accounts: Map[MemberId, Account] = Map()

  def dump() = {
    accounts.foreach(p => println(s"${p._1} ${p._2.purchase}"))
  }
}
