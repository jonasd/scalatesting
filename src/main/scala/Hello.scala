object Hello extends App {


  def multi (n: List[Int], operand: (Int,Int) => Int): Int = {
    n match {
      case x :: tail => operand(x, multi(tail, operand))
      case Nil => 1
    }
  }
  def sum(xs: List[Int]): Int = {
    xs match {
      case x :: tail => x + sum(tail) // if there is an element, add it to the sum of the tail
      case Nil => 0 // if there are no elements, then the sum is 0
    }
  }

  def myOperand (a: Int, b:Int): Int = {
    a * b
  }

  val theSum = multi(List(1,2,3,4), myOperand)
  println("sum1: " + theSum)


  val addList = List(1,2,3,4)
  val theSum2 = sum(List(1,2,3,4))
  println("sum2: " + theSum2)
  println("sum3: " + (1 /: addList) (_*_))
  println("sum3: " + (addList:\1) (_*_))

  def f(x: => Int) = {x * x}

  var y = 3;
  println (f {y = y*3+1;y})
  println (f {3})
  println( f(3))
  println( f {(addList:\1) (_*_)})
}
