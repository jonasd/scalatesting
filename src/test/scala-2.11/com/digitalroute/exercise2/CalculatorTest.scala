package com.digitalroute.exercise2

import org.scalatest.FunSuite

class CalculatorTest extends FunSuite {

   test("no discount below limit") {
     val calculator = new Calculator(0)

     val amountToPay: BigDecimal = calculator.amountToPay(50)

     assert(amountToPay === 50)
   }

   test("full discount") {
     val calculator = new Calculator(150)

     val amountToPay: Double = calculator.amountToPay(50)
     assert(amountToPay === 45)
   }

   test("some discount") {
     val calculator = new Calculator(90)

     val amountToPay: Double = calculator.amountToPay(60)
     assert(amountToPay === 55)
   }

 }