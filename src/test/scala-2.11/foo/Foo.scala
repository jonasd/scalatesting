package foo

import org.scalatest.FlatSpec

class Foo {
  val a = 1
  val b = 1
}

class FooTest extends FlatSpec {

  "A Foo" should "a = 1" in {
    assert(new Foo().a == 1)
  }
}